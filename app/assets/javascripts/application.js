document.addEventListener("trix-file-accept", function(event) {
    const acceptedTypes = ["image/jpeg", "image/png"];
    if (!acceptedTypes.includes(event.file.type)) {
      event.preventDefault();
      alert("Only JPEG and PNG files are allowed");
    }
  });