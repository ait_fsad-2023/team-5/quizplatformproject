class ApplicationController < ActionController::Base

    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:role])
        # Add the :role to the account_update if you wish to allow users to update their role
        # devise_parameter_sanitizer.permit(:account_update, keys: [:role])
        devise_parameter_sanitizer.permit(:account_update, keys: [:username])
    end

end
