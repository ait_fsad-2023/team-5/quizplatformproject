class LearningResourceTransactionsController < ApplicationController
  before_action :set_learning_resource_transaction, only: %i[ show edit update destroy ]

  # GET /learning_resource_transactions or /learning_resource_transactions.json
  def index
    @learning_resource_transactions = LearningResourceTransaction.all
  end

  # GET /learning_resource_transactions/1 or /learning_resource_transactions/1.json
  def show
  end

  # GET /learning_resource_transactions/new
  def new
    @learning_resource_transaction = LearningResourceTransaction.new
  end

  # GET /learning_resource_transactions/1/edit
  def edit
  end

  # POST /learning_resource_transactions or /learning_resource_transactions.json
  def create
    @learning_resource_transaction = LearningResourceTransaction.new(learning_resource_transaction_params)

    respond_to do |format|
      if @learning_resource_transaction.save
        format.html { redirect_to learning_resource_transaction_url(@learning_resource_transaction), notice: "Learning resource transaction was successfully created." }
        format.json { render :show, status: :created, location: @learning_resource_transaction }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @learning_resource_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /learning_resource_transactions/1 or /learning_resource_transactions/1.json
  def update
    respond_to do |format|
      if @learning_resource_transaction.update(learning_resource_transaction_params)
        format.html { redirect_to learning_resource_transaction_url(@learning_resource_transaction), notice: "Learning resource transaction was successfully updated." }
        format.json { render :show, status: :ok, location: @learning_resource_transaction }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @learning_resource_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /learning_resource_transactions/1 or /learning_resource_transactions/1.json
  def destroy
    @learning_resource_transaction.destroy!

    respond_to do |format|
      format.html { redirect_to learning_resource_transactions_url, notice: "Learning resource transaction was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_learning_resource_transaction
      @learning_resource_transaction = LearningResourceTransaction.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def learning_resource_transaction_params
      params.fetch(:learning_resource_transaction, {})
    end
end
