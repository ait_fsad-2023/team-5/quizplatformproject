class LearningResourcesController < ApplicationController
  before_action :set_learning_resource, only: %i[ show edit update destroy ]

  # GET /learning_resources or /learning_resources.json
  def index
    @learning_resources = LearningResource.all
  end

  # GET /learning_resources/1 or /learning_resources/1.json
  def show
    @learning_resource = LearningResource.find(params[:id])
  end

  # GET /learning_resources/new
  def new
    @learning_resource = LearningResource.new
  end

  # GET /learning_resources/1/edit
  def edit
    @learning_resource = LearningResource.find(params[:id])
  end

  # POST /learning_resources or /learning_resources.json
  def create
    @learning_resource = LearningResource.new(learning_resource_params)

    respond_to do |format|
      if @learning_resource.save
        format.html { redirect_to learning_resource_url(@learning_resource), notice: "Learning resource was successfully created." }
        format.json { render :show, status: :created, location: @learning_resource }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @learning_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /learning_resources/1 or /learning_resources/1.json
  def update
    respond_to do |format|
      if @learning_resource.update(learning_resource_params)
        format.html { redirect_to learning_resource_url(@learning_resource), notice: "Learning resource was successfully updated." }
        format.json { render :show, status: :ok, location: @learning_resource }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @learning_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /learning_resources/1 or /learning_resources/1.json
  def destroy
    @learning_resource.destroy!

    respond_to do |format|
      format.html { redirect_to learning_resources_url, notice: "Learning resource was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_learning_resource
      @learning_resource = LearningResource.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    # def learning_resource_params
    #   params.fetch(:learning_resource, {})
    # end
    def learning_resource_params
      params.require(:learning_resource).permit(:topic, :youtube_link, :knowledge, :thumbnail)
    end
end
