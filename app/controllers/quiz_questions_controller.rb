class QuizQuestionsController < ApplicationController
  before_action :set_quiz_question, only: %i[ show edit update destroy ]

  # GET /quiz_questions or /quiz_questions.json
  def index
    @quiz_questions = QuizQuestion.all
  end

  # GET /quiz_questions/1 or /quiz_questions/1.json
  def show
  end

  # GET /quiz_questions/new
  def new
    @quiz_question = QuizQuestion.new
    6.times { @quiz_question.answers.build }
  end

  # GET /quiz_questions/1/edit
  def edit
  end

  # POST /quiz_questions or /quiz_questions.json
  # def create
  #   @quiz_question = QuizQuestion.new(quiz_question_params)

  #   respond_to do |format|
  #     if @quiz_question.save
  #       format.html { redirect_to quiz_question_url(@quiz_question), notice: "Quiz question was successfully created." }
  #       format.json { render :show, status: :created, location: @quiz_question }
  #     else
  #       format.html { render :new, status: :unprocessable_entity }
  #       format.json { render json: @quiz_question.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def create
    @quiz_question = QuizQuestion.new(quiz_question_params)
  
    if @quiz_question.save
      redirect_to quiz_questions_path, notice: "Quiz question was successfully created."
    else
      render :new
    end
  end
  

  # PATCH/PUT /quiz_questions/1 or /quiz_questions/1.json
  def update
    respond_to do |format|
      if @quiz_question.update(quiz_question_params)
        format.html { redirect_to quiz_question_url(@quiz_question), notice: "Quiz question was successfully updated." }
        format.json { render :show, status: :ok, location: @quiz_question }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @quiz_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quiz_questions/1 or /quiz_questions/1.json
  def destroy
    @quiz_question.destroy!

    respond_to do |format|
      format.html { redirect_to quiz_questions_url, notice: "Quiz question was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quiz_question
      @quiz_question = QuizQuestion.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def quiz_question_params
      params.require(:quiz_question).permit(
        *(1..6).map { |i| ["item#{i}", "correct#{i}", "answer#{i}_1", "answer#{i}_2", "answer#{i}_3"] }.flatten
      ).tap do |whitelisted|
        (1..6).each do |i|
          correct_answer = whitelisted["correct#{i}"].to_i
          answer_choices = (1..3).map { |j| whitelisted["answer#{i}_#{j}"] }
          whitelisted["correct#{i}"] = { question: whitelisted["item#{i}"], answers: answer_choices.map.with_index { |answer, index| { answer: answer, isCorrect: (index + 1) == correct_answer } } }.to_json
        end
      end
    end
end
