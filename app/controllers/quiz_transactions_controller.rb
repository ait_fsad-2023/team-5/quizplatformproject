class QuizTransactionsController < ApplicationController
  before_action :set_quiz_transaction, only: %i[ show edit update destroy ]

  # GET /quiz_transactions or /quiz_transactions.json
  def index
    @quiz_transactions = QuizTransaction.all
  end

  # GET /quiz_transactions/1 or /quiz_transactions/1.json
  def show
  end

  # GET /quiz_transactions/new
  def new
    @quiz_transaction = QuizTransaction.new
  end

  # GET /quiz_transactions/1/edit
  def edit
  end

  # POST /quiz_transactions or /quiz_transactions.json
  def create
    @quiz_transaction = QuizTransaction.new(quiz_transaction_params)

    respond_to do |format|
      if @quiz_transaction.save
        format.html { redirect_to quiz_transaction_url(@quiz_transaction), notice: "Quiz transaction was successfully created." }
        format.json { render :show, status: :created, location: @quiz_transaction }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @quiz_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quiz_transactions/1 or /quiz_transactions/1.json
  def update
    respond_to do |format|
      if @quiz_transaction.update(quiz_transaction_params)
        format.html { redirect_to quiz_transaction_url(@quiz_transaction), notice: "Quiz transaction was successfully updated." }
        format.json { render :show, status: :ok, location: @quiz_transaction }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @quiz_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quiz_transactions/1 or /quiz_transactions/1.json
  def destroy
    @quiz_transaction.destroy!

    respond_to do |format|
      format.html { redirect_to quiz_transactions_url, notice: "Quiz transaction was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quiz_transaction
      @quiz_transaction = QuizTransaction.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def quiz_transaction_params
      params.fetch(:quiz_transaction, {})
    end
end
