class LearningResource < ApplicationRecord
    has_many :learning_resource_transactions
    validates :topic, presence: true
    has_one_attached :thumbnail
end
