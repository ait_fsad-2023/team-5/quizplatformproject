class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum role: { student: 0, teacher: 1, admin: 2 }

  has_many :learning_resource_transactions
  has_many :quiz_transactions

  before_save :assign_default_role, if: :new_record?
  # before_save :prevent_unauthorized_role_assignment

  private

  def assign_default_role
    self.role ||= :student
  end

  def prevent_unauthorized_role_assignment
    # For example, only allow admins to assign roles or prevent role changes unless it's an admin
    if role_changed? && !admin?
      errors.add(:role, "You are not authorized to change roles.")
      throw(:abort)
    end
  end

end
