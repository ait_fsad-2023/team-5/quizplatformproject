json.extract! learning_resource_transaction, :id, :created_at, :updated_at
json.url learning_resource_transaction_url(learning_resource_transaction, format: :json)
