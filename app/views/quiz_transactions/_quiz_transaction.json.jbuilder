json.extract! quiz_transaction, :id, :created_at, :updated_at
json.url quiz_transaction_url(quiz_transaction, format: :json)
