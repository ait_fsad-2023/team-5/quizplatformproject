Rails.application.routes.draw do
  root 'learning_resources#index'  
  resources :quiz_transactions
  resources :learning_resource_transactions
  resources :quiz_questions
  resources :learning_resources
  devise_for :users

  resources :quizzes do
    resources :questions
  end

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check
  
  # Defines the root path route ("/")
  # root "posts#index"
end
