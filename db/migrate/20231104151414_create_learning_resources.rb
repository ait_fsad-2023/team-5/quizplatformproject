class CreateLearningResources < ActiveRecord::Migration[7.1]
  def change
    create_table :learning_resources do |t|
      t.string :youtube_link
      t.text :knowledge

      t.timestamps
    end
  end
end
