class CreateQuizQuestions < ActiveRecord::Migration[7.1]
  def change
    create_table :quiz_questions do |t|
      t.text :item1
      t.text :item2
      t.text :item3
      t.text :item4
      t.text :item5
      t.text :item6
      t.integer :correct1
      t.integer :correct2
      t.integer :correct3
      t.integer :correct4
      t.integer :correct5
      t.integer :correct6

      t.timestamps
    end
  end
end
