class CreateLearningResourceTransactions < ActiveRecord::Migration[7.1]
  def change
    create_table :learning_resource_transactions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :learning_resource, null: false, foreign_key: true
      t.boolean :completed

      t.timestamps
    end
  end
end
