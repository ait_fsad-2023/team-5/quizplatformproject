class CreateQuizTransactions < ActiveRecord::Migration[7.1]
  def change
    create_table :quiz_transactions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :quiz_question, null: false, foreign_key: true
      t.integer :marks

      t.timestamps
    end
  end
end
