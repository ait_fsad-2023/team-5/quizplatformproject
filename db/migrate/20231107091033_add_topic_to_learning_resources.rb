class AddTopicToLearningResources < ActiveRecord::Migration[7.1]
  def change
    add_column :learning_resources, :topic, :string
  end
end
