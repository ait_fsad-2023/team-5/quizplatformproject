class AddThumbnailToLearningResources < ActiveRecord::Migration[7.1]
  def change
    add_column :learning_resources, :thumbnail, :string
  end
end
