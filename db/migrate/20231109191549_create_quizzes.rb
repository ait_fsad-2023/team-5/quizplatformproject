class CreateQuizzes < ActiveRecord::Migration[7.1]
  def change
    create_table :quizzes do |t|
      t.string :title
      t.string :question1
      t.string :anstext1
      t.boolean :anstext_boolean11
      t.boolean :anstext_boolean12
      t.boolean :anstext_boolean13
      t.string :question2
      t.string :anstext2
      t.boolean :anstext_boolean21
      t.boolean :anstext_boolean22
      t.boolean :anstext_boolean23
      t.string :question3
      t.string :anstext3
      t.boolean :anstext_boolean31
      t.boolean :anstext_boolean32
      t.boolean :anstext_boolean33
      t.string :question4
      t.string :anstext4
      t.boolean :anstext_boolean41
      t.boolean :anstext_boolean42
      t.boolean :anstext_boolean43
      t.string :question5
      t.string :anstext5
      t.boolean :anstext_boolean51
      t.boolean :anstext_boolean52
      t.boolean :anstext_boolean53
      t.string :question6
      t.string :anstext6
      t.boolean :anstext_boolean61
      t.boolean :anstext_boolean62
      t.boolean :anstext_boolean63

      t.timestamps
    end
  end
end
