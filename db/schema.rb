# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2023_11_09_191549) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "learning_resource_transactions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "learning_resource_id", null: false
    t.boolean "completed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["learning_resource_id"], name: "index_learning_resource_transactions_on_learning_resource_id"
    t.index ["user_id"], name: "index_learning_resource_transactions_on_user_id"
  end

  create_table "learning_resources", force: :cascade do |t|
    t.string "youtube_link"
    t.text "knowledge"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "topic"
    t.string "thumbnail"
  end

  create_table "quiz_questions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "content"
  end

  create_table "quiz_transactions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "quiz_question_id", null: false
    t.integer "marks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["quiz_question_id"], name: "index_quiz_transactions_on_quiz_question_id"
    t.index ["user_id"], name: "index_quiz_transactions_on_user_id"
  end

  create_table "quizzes", force: :cascade do |t|
    t.string "title"
    t.string "question1"
    t.string "anstext1"
    t.boolean "anstext_boolean11"
    t.boolean "anstext_boolean12"
    t.boolean "anstext_boolean13"
    t.string "question2"
    t.string "anstext2"
    t.boolean "anstext_boolean21"
    t.boolean "anstext_boolean22"
    t.boolean "anstext_boolean23"
    t.string "question3"
    t.string "anstext3"
    t.boolean "anstext_boolean31"
    t.boolean "anstext_boolean32"
    t.boolean "anstext_boolean33"
    t.string "question4"
    t.string "anstext4"
    t.boolean "anstext_boolean41"
    t.boolean "anstext_boolean42"
    t.boolean "anstext_boolean43"
    t.string "question5"
    t.string "anstext5"
    t.boolean "anstext_boolean51"
    t.boolean "anstext_boolean52"
    t.boolean "anstext_boolean53"
    t.string "question6"
    t.string "anstext6"
    t.boolean "anstext_boolean61"
    t.boolean "anstext_boolean62"
    t.boolean "anstext_boolean63"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role", default: 0
    t.string "username"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "learning_resource_transactions", "learning_resources"
  add_foreign_key "learning_resource_transactions", "users"
  add_foreign_key "quiz_transactions", "quiz_questions"
  add_foreign_key "quiz_transactions", "users"
end
