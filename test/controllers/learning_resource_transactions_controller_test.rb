require "test_helper"

class LearningResourceTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @learning_resource_transaction = learning_resource_transactions(:one)
  end

  test "should get index" do
    get learning_resource_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_learning_resource_transaction_url
    assert_response :success
  end

  test "should create learning_resource_transaction" do
    assert_difference("LearningResourceTransaction.count") do
      post learning_resource_transactions_url, params: { learning_resource_transaction: {  } }
    end

    assert_redirected_to learning_resource_transaction_url(LearningResourceTransaction.last)
  end

  test "should show learning_resource_transaction" do
    get learning_resource_transaction_url(@learning_resource_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_learning_resource_transaction_url(@learning_resource_transaction)
    assert_response :success
  end

  test "should update learning_resource_transaction" do
    patch learning_resource_transaction_url(@learning_resource_transaction), params: { learning_resource_transaction: {  } }
    assert_redirected_to learning_resource_transaction_url(@learning_resource_transaction)
  end

  test "should destroy learning_resource_transaction" do
    assert_difference("LearningResourceTransaction.count", -1) do
      delete learning_resource_transaction_url(@learning_resource_transaction)
    end

    assert_redirected_to learning_resource_transactions_url
  end
end
