require "test_helper"

class QuizTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quiz_transaction = quiz_transactions(:one)
  end

  test "should get index" do
    get quiz_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_quiz_transaction_url
    assert_response :success
  end

  test "should create quiz_transaction" do
    assert_difference("QuizTransaction.count") do
      post quiz_transactions_url, params: { quiz_transaction: {  } }
    end

    assert_redirected_to quiz_transaction_url(QuizTransaction.last)
  end

  test "should show quiz_transaction" do
    get quiz_transaction_url(@quiz_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_quiz_transaction_url(@quiz_transaction)
    assert_response :success
  end

  test "should update quiz_transaction" do
    patch quiz_transaction_url(@quiz_transaction), params: { quiz_transaction: {  } }
    assert_redirected_to quiz_transaction_url(@quiz_transaction)
  end

  test "should destroy quiz_transaction" do
    assert_difference("QuizTransaction.count", -1) do
      delete quiz_transaction_url(@quiz_transaction)
    end

    assert_redirected_to quiz_transactions_url
  end
end
