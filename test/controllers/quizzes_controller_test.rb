require "test_helper"

class QuizzesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quiz = quizzes(:one)
  end

  test "should get index" do
    get quizzes_url
    assert_response :success
  end

  test "should get new" do
    get new_quiz_url
    assert_response :success
  end

  test "should create quiz" do
    assert_difference("Quiz.count") do
      post quizzes_url, params: { quiz: { anstext1: @quiz.anstext1, anstext2: @quiz.anstext2, anstext3: @quiz.anstext3, anstext4: @quiz.anstext4, anstext5: @quiz.anstext5, anstext6: @quiz.anstext6, anstext_boolean11: @quiz.anstext_boolean11, anstext_boolean12: @quiz.anstext_boolean12, anstext_boolean13: @quiz.anstext_boolean13, anstext_boolean21: @quiz.anstext_boolean21, anstext_boolean22: @quiz.anstext_boolean22, anstext_boolean23: @quiz.anstext_boolean23, anstext_boolean31: @quiz.anstext_boolean31, anstext_boolean32: @quiz.anstext_boolean32, anstext_boolean33: @quiz.anstext_boolean33, anstext_boolean41: @quiz.anstext_boolean41, anstext_boolean42: @quiz.anstext_boolean42, anstext_boolean43: @quiz.anstext_boolean43, anstext_boolean51: @quiz.anstext_boolean51, anstext_boolean52: @quiz.anstext_boolean52, anstext_boolean53: @quiz.anstext_boolean53, anstext_boolean61: @quiz.anstext_boolean61, anstext_boolean62: @quiz.anstext_boolean62, anstext_boolean63: @quiz.anstext_boolean63, question1: @quiz.question1, question2: @quiz.question2, question3: @quiz.question3, question4: @quiz.question4, question5: @quiz.question5, question6: @quiz.question6, title: @quiz.title } }
    end

    assert_redirected_to quiz_url(Quiz.last)
  end

  test "should show quiz" do
    get quiz_url(@quiz)
    assert_response :success
  end

  test "should get edit" do
    get edit_quiz_url(@quiz)
    assert_response :success
  end

  test "should update quiz" do
    patch quiz_url(@quiz), params: { quiz: { anstext1: @quiz.anstext1, anstext2: @quiz.anstext2, anstext3: @quiz.anstext3, anstext4: @quiz.anstext4, anstext5: @quiz.anstext5, anstext6: @quiz.anstext6, anstext_boolean11: @quiz.anstext_boolean11, anstext_boolean12: @quiz.anstext_boolean12, anstext_boolean13: @quiz.anstext_boolean13, anstext_boolean21: @quiz.anstext_boolean21, anstext_boolean22: @quiz.anstext_boolean22, anstext_boolean23: @quiz.anstext_boolean23, anstext_boolean31: @quiz.anstext_boolean31, anstext_boolean32: @quiz.anstext_boolean32, anstext_boolean33: @quiz.anstext_boolean33, anstext_boolean41: @quiz.anstext_boolean41, anstext_boolean42: @quiz.anstext_boolean42, anstext_boolean43: @quiz.anstext_boolean43, anstext_boolean51: @quiz.anstext_boolean51, anstext_boolean52: @quiz.anstext_boolean52, anstext_boolean53: @quiz.anstext_boolean53, anstext_boolean61: @quiz.anstext_boolean61, anstext_boolean62: @quiz.anstext_boolean62, anstext_boolean63: @quiz.anstext_boolean63, question1: @quiz.question1, question2: @quiz.question2, question3: @quiz.question3, question4: @quiz.question4, question5: @quiz.question5, question6: @quiz.question6, title: @quiz.title } }
    assert_redirected_to quiz_url(@quiz)
  end

  test "should destroy quiz" do
    assert_difference("Quiz.count", -1) do
      delete quiz_url(@quiz)
    end

    assert_redirected_to quizzes_url
  end
end
