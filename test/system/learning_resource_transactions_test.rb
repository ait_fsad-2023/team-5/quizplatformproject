require "application_system_test_case"

class LearningResourceTransactionsTest < ApplicationSystemTestCase
  setup do
    @learning_resource_transaction = learning_resource_transactions(:one)
  end

  test "visiting the index" do
    visit learning_resource_transactions_url
    assert_selector "h1", text: "Learning resource transactions"
  end

  test "should create learning resource transaction" do
    visit learning_resource_transactions_url
    click_on "New learning resource transaction"

    click_on "Create Learning resource transaction"

    assert_text "Learning resource transaction was successfully created"
    click_on "Back"
  end

  test "should update Learning resource transaction" do
    visit learning_resource_transaction_url(@learning_resource_transaction)
    click_on "Edit this learning resource transaction", match: :first

    click_on "Update Learning resource transaction"

    assert_text "Learning resource transaction was successfully updated"
    click_on "Back"
  end

  test "should destroy Learning resource transaction" do
    visit learning_resource_transaction_url(@learning_resource_transaction)
    click_on "Destroy this learning resource transaction", match: :first

    assert_text "Learning resource transaction was successfully destroyed"
  end
end
