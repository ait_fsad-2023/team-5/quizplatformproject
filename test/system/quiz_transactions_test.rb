require "application_system_test_case"

class QuizTransactionsTest < ApplicationSystemTestCase
  setup do
    @quiz_transaction = quiz_transactions(:one)
  end

  test "visiting the index" do
    visit quiz_transactions_url
    assert_selector "h1", text: "Quiz transactions"
  end

  test "should create quiz transaction" do
    visit quiz_transactions_url
    click_on "New quiz transaction"

    click_on "Create Quiz transaction"

    assert_text "Quiz transaction was successfully created"
    click_on "Back"
  end

  test "should update Quiz transaction" do
    visit quiz_transaction_url(@quiz_transaction)
    click_on "Edit this quiz transaction", match: :first

    click_on "Update Quiz transaction"

    assert_text "Quiz transaction was successfully updated"
    click_on "Back"
  end

  test "should destroy Quiz transaction" do
    visit quiz_transaction_url(@quiz_transaction)
    click_on "Destroy this quiz transaction", match: :first

    assert_text "Quiz transaction was successfully destroyed"
  end
end
