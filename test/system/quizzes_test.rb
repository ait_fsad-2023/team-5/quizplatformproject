require "application_system_test_case"

class QuizzesTest < ApplicationSystemTestCase
  setup do
    @quiz = quizzes(:one)
  end

  test "visiting the index" do
    visit quizzes_url
    assert_selector "h1", text: "Quizzes"
  end

  test "should create quiz" do
    visit quizzes_url
    click_on "New quiz"

    fill_in "Anstext1", with: @quiz.anstext1
    fill_in "Anstext2", with: @quiz.anstext2
    fill_in "Anstext3", with: @quiz.anstext3
    fill_in "Anstext4", with: @quiz.anstext4
    fill_in "Anstext5", with: @quiz.anstext5
    fill_in "Anstext6", with: @quiz.anstext6
    check "Anstext boolean11" if @quiz.anstext_boolean11
    check "Anstext boolean12" if @quiz.anstext_boolean12
    check "Anstext boolean13" if @quiz.anstext_boolean13
    check "Anstext boolean21" if @quiz.anstext_boolean21
    check "Anstext boolean22" if @quiz.anstext_boolean22
    check "Anstext boolean23" if @quiz.anstext_boolean23
    check "Anstext boolean31" if @quiz.anstext_boolean31
    check "Anstext boolean32" if @quiz.anstext_boolean32
    check "Anstext boolean33" if @quiz.anstext_boolean33
    check "Anstext boolean41" if @quiz.anstext_boolean41
    check "Anstext boolean42" if @quiz.anstext_boolean42
    check "Anstext boolean43" if @quiz.anstext_boolean43
    check "Anstext boolean51" if @quiz.anstext_boolean51
    check "Anstext boolean52" if @quiz.anstext_boolean52
    check "Anstext boolean53" if @quiz.anstext_boolean53
    check "Anstext boolean61" if @quiz.anstext_boolean61
    check "Anstext boolean62" if @quiz.anstext_boolean62
    check "Anstext boolean63" if @quiz.anstext_boolean63
    fill_in "Question1", with: @quiz.question1
    fill_in "Question2", with: @quiz.question2
    fill_in "Question3", with: @quiz.question3
    fill_in "Question4", with: @quiz.question4
    fill_in "Question5", with: @quiz.question5
    fill_in "Question6", with: @quiz.question6
    fill_in "Title", with: @quiz.title
    click_on "Create Quiz"

    assert_text "Quiz was successfully created"
    click_on "Back"
  end

  test "should update Quiz" do
    visit quiz_url(@quiz)
    click_on "Edit this quiz", match: :first

    fill_in "Anstext1", with: @quiz.anstext1
    fill_in "Anstext2", with: @quiz.anstext2
    fill_in "Anstext3", with: @quiz.anstext3
    fill_in "Anstext4", with: @quiz.anstext4
    fill_in "Anstext5", with: @quiz.anstext5
    fill_in "Anstext6", with: @quiz.anstext6
    check "Anstext boolean11" if @quiz.anstext_boolean11
    check "Anstext boolean12" if @quiz.anstext_boolean12
    check "Anstext boolean13" if @quiz.anstext_boolean13
    check "Anstext boolean21" if @quiz.anstext_boolean21
    check "Anstext boolean22" if @quiz.anstext_boolean22
    check "Anstext boolean23" if @quiz.anstext_boolean23
    check "Anstext boolean31" if @quiz.anstext_boolean31
    check "Anstext boolean32" if @quiz.anstext_boolean32
    check "Anstext boolean33" if @quiz.anstext_boolean33
    check "Anstext boolean41" if @quiz.anstext_boolean41
    check "Anstext boolean42" if @quiz.anstext_boolean42
    check "Anstext boolean43" if @quiz.anstext_boolean43
    check "Anstext boolean51" if @quiz.anstext_boolean51
    check "Anstext boolean52" if @quiz.anstext_boolean52
    check "Anstext boolean53" if @quiz.anstext_boolean53
    check "Anstext boolean61" if @quiz.anstext_boolean61
    check "Anstext boolean62" if @quiz.anstext_boolean62
    check "Anstext boolean63" if @quiz.anstext_boolean63
    fill_in "Question1", with: @quiz.question1
    fill_in "Question2", with: @quiz.question2
    fill_in "Question3", with: @quiz.question3
    fill_in "Question4", with: @quiz.question4
    fill_in "Question5", with: @quiz.question5
    fill_in "Question6", with: @quiz.question6
    fill_in "Title", with: @quiz.title
    click_on "Update Quiz"

    assert_text "Quiz was successfully updated"
    click_on "Back"
  end

  test "should destroy Quiz" do
    visit quiz_url(@quiz)
    click_on "Destroy this quiz", match: :first

    assert_text "Quiz was successfully destroyed"
  end
end
